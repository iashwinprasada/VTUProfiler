import re
import mechanize
from bs4 import BeautifulSoup
import logging

class student_VA:
    """Get Sem's data from server"""
    def __init__(self):
        self.stuName= ""
        self.sem5 = []
        self.sem6 = []
        self.sem7 = []
        self.sem8 = []

    def remove_html_tags(self,data):
        p = re.compile(r'<.*?>')
        return p.sub('', data)

    def GetTheResults(self,usn,sem):
        ua = 'Mozilla/5.0 (X11; Linux x86_64; rv:18.0) Gecko/20100101 Firefox/18.0 (compatible;)'
        br = mechanize.Browser()
        br.set_debug_http(True)
        br.set_debug_responses(True)
        br.addheaders = [('User-Agent', ua), ('Accept', '*/*')]
        br.set_handle_robots(False)

        usn_stu = usn
        # last4
        link = "http://results.vtualerts.com/get_res.php?usn=" + usn + "&sem=" + str(sem)

        print link

        x = br.open(link)
        soup = BeautifulSoup(br.response().read())
        table = soup.findAll("table")
        t2trP = table[2].prettify()
        t2trsoup = BeautifulSoup(t2trP)

        if sem == 5:
            print "inside 5 col[]" + "\n"
            for row in t2trsoup.findAll('tr'):
              self.sem5.append(row.findAll('td'))
            # Print
            self.get_sem_list(5)
        elif sem == 6:
            print "inside 6 col[]" + "\n"
            for row in t2trsoup.findAll('tr'):
              self.sem6.append(row.findAll('td'))
            self.get_sem_list(6)
        elif sem == 7:
            for row in t2trsoup.findAll('tr'):
              self.sem7.append(row.findAll('td'))
            self.get_sem_list(7)
        elif sem == 8:
            for row in t2trsoup.findAll('tr'):
              self.sem8.append(row.findAll('td'))
            self.get_sem_list(8)

    def get_sem_list(self,sem):
        subjects_vtu = []
        marks_ext = []
        marks_int = []

        if sem == 5:
            for i in range(1,9):
                print "inside 5 semlist"
                #print "Subject \n"
                #print "Marks \n"
                subjects_vtu.append(self.remove_html_tags(str(self.sem5[i][0])))
                marks_ext.append(self.remove_html_tags(str(self.sem5[i][1])))
                marks_int.append(self.remove_html_tags(str(self.sem5[i][2])))

        elif sem == 6:
            for i in range(1,9):
                print "inside 6 sem"
                #print "Subject \n"
                #print "Marks \n"
                subjects_vtu.append(self.remove_html_tags(str(self.sem6[i][0])))
                marks_ext.append(self.remove_html_tags(str(self.sem6[i][1])))
                marks_int.append(self.remove_html_tags(str(self.sem6[i][2])))

        elif sem == 7:
            for i in range(1,9):
                #print "Subject \n"
                #print "Marks \n"
                subjects_vtu.append(self.remove_html_tags(str(self.sem7[i][0])))
                marks_ext.append(self.remove_html_tags(str(self.sem7[i][1])))
                marks_int.append(self.remove_html_tags(str(self.sem7[i][2])))

        elif sem == 8:
            for i in range(1,7):
                #print "Subject \n"
                #print "Marks \n"
                subjects_vtu.append(self.remove_html_tags(str(self.sem8[i][0])))
                marks_ext.append(self.remove_html_tags(str(self.sem8[i][1])))
                marks_int.append(self.remove_html_tags(str(self.sem8[i][2])))

        else:
            return "not working!"

        # print "Subjects \n"
        # subjects_vtu = [item.strip() for item in subjects_vtu if str(item)]
        # print '[%s]' % ', '.join(map(str, subjects_vtu))
        # print "\n"
        #
        # print "External Marks"
        # marks_ext = [item.strip() for item in marks_ext if str(item)]
        # print '[%s]' % ', '.join(map(str, marks_ext))
        # print "\n"
        # print "Internal Marks"
        # marks_int = [item.strip() for item in marks_int if str(item)]
        # print '[%s]' % ', '.join(map(str, marks_int))
        # print "\n"

        return subjects_vtu, marks_ext, marks_int


if __name__ == "__main__":
    student = student_VA()
    usn = raw_input()
    for i in range(5,9):
        student.GetTheResults(usn,i)
